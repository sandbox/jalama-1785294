<?php
/**
 * Implementation of hook_views_plugins
 */
function views_unionize_views_plugins() {
  return array(
    'display' => array(
      'unionized' => array(
        'title' => t('Union'),
        'help' => t(''),
        'path' =>drupal_get_path('module', 'bigjim_views_unionize') . '/plugins',
        'handler' => 'bigjim_views_unionize_plugin_display_union',
        'use ajax' => FALSE,
        'use pager' => TRUE,
        'admin' => t('Unionized')
        // @TODO - Should probably provide some help.
        // 'help topic' => 'display-feed',
      )
    )
  );
}


/**
 *
 * deal with child query field replacements
 *  strip ambiguous fields from field list, since field names won't be specific to tables
 *  @TO-DO: deal with AS alias identifiers with a custom approach to naming to somehow keep all fields available
 *
 */
function views_unionize_getField(&$fields, $prefix, $field_name) {
  $name = preg_replace('/^.*?\./', '', $field_name);
  if (array_key_exists($name,$fields)) {
    // overwrite existing alias (last one set gets precedence)
    $fields[$name]['full'] = $field_name;
    // maybe we'll return a count increment on alias
    return $prefix . $field_name . " AS " . $name . ++$fields[$name]['count'];
  } else {
    $fields[$name] = array('full'=>$field_name, 'count'=>0);
  }
  return $prefix . $field_name . " AS " . $name;
}

function views_unionize_views_pre_build($view) {
  $display_id = (is_array($view->current_display) ? $view->current_display[0] : $view->current_display);
  // pass through arguments to children, based on value of union argument
  if ($view->display[$display_id]->handler->options['expose_arguments']) {
    $view->args_cache = $view->args;
    $view->set_arguments(null);
    $view->argument = null;
  }
}

/**
 * this function is run within views::execute()
 *
 * @var view $view
 */
function views_unionize_views_pre_execute($view) {
  $display_id = (is_array($view->current_display) ? $view->current_display[0] : $view->current_display);
  if(preg_match('/union/i', $display_id)){
  $union = array();
  $handler = $view->display[$display_id]->handler;

  foreach($handler->options['displays'] as $id => $set) {
    $sub_view = views_get_view($view->name);
    if ($set && !preg_match('/union/i', $sub_view->current_display)) {

      // pass through arguments to children, bsaed on value of union argument
      if ($handler->options['expose_arguments']) {
        $sub_view->set_arguments($view->args_cache);
        $view->set_arguments(null);
        $view->argument = null;
      }

      foreach ($sub_view->display as $key=>$val) {
        $fields = &$sub_view->display[$key]->display_options['fields'];
        unset ($fields['title']);
      }

      // now run build on the display to get the query information
      $sub_view->execute($id);

      // Now replace inner query aliases so they continue to work as nested queries
      $fields = array();

      // field name replacement: \1 is prefix, \2 is field_name
      $sub_view->build_info['query'] = preg_replace('/([\W]+)(.*?) AS [\w]+/e', "views_unionize_getField(\$fields,'\\1','\\2')",
        $sub_view->build_info['query']);

      $field_list = array();
      foreach ($fields as $alias => $field_data) {
        array_push($field_list, $fields[$alias]['full'] . " AS " . $alias);
      }

      $sub_view->build_info['query'] = preg_replace(
        '/SELECT(.*?)FROM([\W]+)/es',
        "'SELECT ' . implode(', ', \$field_list) . ' FROM\\2'",
        $sub_view->build_info['query']);

      $base_table = $view->base_table;
      $base_table_alias = $base_table . (count($union)+1);

      $args = $sub_view->build_info['query_args'];
      array_unshift($args, $sub_view->build_info['query']);

      // determine if query info should be pushed onto union array
      if ($sub_view->build_info['query'])
        $union[] = call_user_func_array('sprintf', $args);
      }
    }
    if (count($union) >= 1)
    {
      $base_table = '(('. implode(') UNION (', $union) .'))';

      $view->build_info['query'] = preg_replace(
        '/\{'. $view->base_table . '\} ' . $view->base_table .'/',
        $base_table . ' ' . $view->base_table,
        $view->build_info['query']
      );
    }
  }
}

