$Id: README.txt,v 1.0.0.0.0.0 2011/03/29 17:15:00 tsimms Exp $

Views Unionize for Drupal 6.x
-----------------------------
Views Unionize creates a new views display type called Union. Union 
displays enable results from display queries to be unioned together prior 
to being displayed. Effectively, they are a simple way to get multiple
view results within the same view.

Think of conditions as a set of rules that are checked during page load
to see what context is active. Any reactions that are associated with
active contexts are then fired.


Installation
------------
Context can be installed like any other Drupal module -- place it in
the modules directory for your site and enable it on the 
`admin/build/modules` page.

There is a pre-requisite of Views and Views UI, but if you've made it
here, chances are you already have views installed anyway.  :)


Usage
-----
Once you install the module, you'll have a new display type within views, 
called "Union". You choose 2 or more displays in the same view that you'd 
like to union together. Special attention should be given to ensure you 
have the same number of database fields in your child displays, otherwise 
you'll get a database UNION error. Keep in mind, not every view field 
constitutes a database field, so you may have some trial-and-error here. 
After that, define some fields in the union display (you should only 
reference fields that exist in the child fields of course; otherwise, 
you'll get errors stating the field you chose cannot be found). Finally, 
you can format the data using fields, sort, filter, or any of the output 
options on the union display.

The main use case we solved with this module was that we had view displays 
that had distinct relationships based on node references, but we wanted 
the results to be sorted by title (which of course existed in the 
different content types we were using) all merged together.


Maintainers
-----------

- tsimms (Tim Simms)
Would welcome the support of other co-mainatainers!
This project is sponsored by <a href="http://www.centreTEK.com">CentreTEK Solutions</a>.

