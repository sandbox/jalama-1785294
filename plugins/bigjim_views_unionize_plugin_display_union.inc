<?php
// $Id: views_plugin_plugin_display_union.inc,v 1.6.2.1 2011/03/21 19:05:47 tsimms Exp $
/**
 * @file
 * Contains the views unionize plugin
 */

/**
 * The plugin that handles a union display.
 *
 * Union displays enable results from display queries to be unioned together
 * prior to being displayed.  Effectively, they are a simple way to get multiple
 * view results within the same view.
 *
 * @ingroup views_display_plugins
 */
class views_unionize_plugin_display_union extends views_plugin_display {
  function option_definition () {
    $options = parent::option_definition();

    $options['expose_arguments'] = array('default' => TRUE);
    $options['displays'] = array('default' => array());

    return $options;
  }

  function execute() {
	return $this->view->render($this->display->id);
  }

  /**
   * Provide the summary for union options in the views UI.
   *
   * This output is returned as an array.
   */
  function options_summary(&$categories, &$options) {
    // It is very important to call the parent function here:
    parent::options_summary($categories, $options);

    $categories['attachment'] = array(
      'title' => t('Union settings'),
    );

    $options['expose_arguments'] = array(
      'category' => 'attachment',
      'title' => t('Expose arguments'),
      'value' => $this->get_option('expose_arguments') ? t('Yes') : t('No'),
    );

    $displays = array_filter($this->get_option('displays'));
    if (count($displays) > 1) {
      $attach_to = t('Multiple displays');
    }
    else if (count($displays) == 1) {
      $display = array_shift($displays);
      if (!empty($this->view->display[$display])) {
        $attach_to = check_plain($this->view->display[$display]->display_title);
      }
    }

    if (!isset($attach_to)) {
      $attach_to = t('None');
    }

    $options['displays'] = array(
      'category' => 'attachment',
      'title' => t('Attach to'),
      'value' => $attach_to,
    );
  }

  /**
   * Provide the default form for setting options.
   */
  function options_form(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_form($form, $form_state);

    switch ($form_state['section']) {
      case 'expose_arguments':
        $form['#title'] .= t('Expose arguments');
        $form['expose_arguments'] = array(
          '#type' => 'checkbox',
          '#title' => t('Expose'),
          '#description' => t('Should this display expose its arguments to other child displays?'),
          '#default_value' => $this->get_option('expose_arguments'),
        );
        break;
      case 'displays':
        $form['#title'] .= t('Attach to');
        $displays = array();
        foreach ($this->view->display as $display_id => $display) {
          if (!empty($display->handler) && $display->handler->accept_attachments()) {
            $displays[$display_id] = $display->display_title;
          }
        }
        $form['displays'] = array(
          '#type' => 'checkboxes',
          '#description' => t('Select which display(s) this parent display should embed.'),
          '#options' => $displays,
          '#default_value' => $this->get_option('displays'),
        );
        break;
    }
  }

  /**
   * Perform any necessary changes to the form values prior to storage.
   * There is no need for this function to actually store the data.
   */
  function options_submit(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_submit($form, $form_state);
    switch ($form_state['section']) {
      case 'expose_arguments':
      case 'displays':
        $this->set_option($form_state['section'], $form_state['values'][$form_state['section']]);
        break;
    }
  }

  function render() {
    /* @TO-DO: Figure out why we can't call theme_functions() to get array of theme functions to render */
    return theme(array("views_view"), $this->view);
  }

}
